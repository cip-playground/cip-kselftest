#!/bin/sh

# TODO move this in a dedicated test
# assume only one network card
ETH=$(ls /sys/class/net |grep -E 'eth|enp')
WORK_DIR=$(pwd)

check_arch() {
  case $(uname -m) in
    aarch64)
      KSARCH=arm64
      echo "Skip kselftest for arm64 because of loop"
      # https://lava.ciplatform.org/scheduler/job/302671
      exit 0
      ;;
    x86_64)
      KSARCH=x86
      echo "Kselftest x86"
      ;;
    armv7l)
      KSARCH=arm
      echo "Kselftest arm"
      ;;
    *)
      echo "SKIP: no kselftests for $(uname -m) arch"
      exit 0
      ;;
  esac
}

start_network() {
  if [ -z "$ETH" ];then
      echo "WARN: no network card found"
  else
      echo "INFO: configure network card $ETH"
      ln -s /etc/init.d/net.lo "/etc/init.d/net.$ETH"
      "/etc/init.d/net.$ETH" start
  fi
}

check_binary() {
  run_kselftest=""
  if [ -e /kselftest/run_kselftest.sh ]; then
    echo "-------------------------------------------"
    echo "Found kselftest overlay at /kselftest/     "
    echo "-------------------------------------------"
    run_kselftest=/kselftest/run_kselftest.sh
  elif [ -e "/opt/kselftest-${KSARCH}.tar.gz" ]; then
    echo "-------------------------------------------"
    echo "Found builded /opt/kselftest-${KSARCH}.tar.gz binary "
    echo "-------------------------------------------"
    tar xzf "/opt/kselftest-${KSARCH}.tar.gz" -C "${WORK_DIR}" || exit $?
    run_kselftest=${WORK_DIR}/kselftest/run_kselftest.sh
  else
    # try to get kselftest from kselftest repository
    if wget http://storage.kernelci.org/images/selftests/$KSARCH/kselftest.tar.gz ; then
      echo "---------------------------------------"
      echo "Couldn't find builded kselftest binary "
      echo "Found dowloaded kselftest binary       "
      echo "---------------------------------------"
      tar xzf kselftest.tar.gz -C "${WORK_DIR}" || exit $?
      run_kselftest=${WORK_DIR}/kselftest/run_kselftest.sh
    else
      echo "---------------------------------------"
      echo "Couldn't find builded kselftest binary "
      echo "Couldn't dowload kselftest binary      "
      echo "Using default prebuilded binary        "
      echo "---------------------------------------"
      tar xzf "${WORK_DIR}/binary/$KSARCH/kselftest.tar.gz" -C "${WORK_DIR}" || exit $?
      run_kselftest=${WORK_DIR}/kselftest/run_kselftest.sh
    fi
  fi
}


check_arch
start_network
check_binary
echo "Check enviroment:"
echo "check WORK_DIR path"
echo "${WORK_DIR}"
echo "check WORK_DIR folder"
ls "${WORK_DIR}" || exit $?
echo "check /opt folder"
ls /opt/ || exit $?
echo "-------------------- start kselftest -------------------"
# run kselftest test
if [ -z "${run_kselftest}" ]; then
  echo "kselftest binary not found"
else
  exec "${run_kselftest}" | tee "${WORK_DIR}/output.txt" || exit $?
  exec "${WORK_DIR}/parser.sh" "${WORK_DIR}/output.txt" || exit $?
fi
echo "------------------ end kselftest -----------------------"

