#!/bin/sh
#set -x

RESULT_FILE="$1"

which lava-test-case > /dev/null 2>&1
lava_test_case="$?"
which lava-test-set > /dev/null 2>&1
lava_test_set="$?"

IFS=$'\r\n' GLOBIGNORE='*' command eval  'XYZ=($(cat ../skiplist.txt))'

if [ -f "${RESULT_FILE}" ]; then
    while read -r line; do
        if echo "${line}" | grep -iq -E "^running tests"; then
          test="$(echo "${line}" | awk '{print $NF}')"
          echo "<TEST IN ${test}>"
        elif echo "${line}" | grep -iq -E "^selftests:.*(pass|fail|skip)]$"; then
            test="$(echo "${line}" | awk '{$NF="";print $0}' | sed 's/selftests: //g' | sed 's/ //g')"
            result="$(echo "${line}" | awk '{print $NF}')"
            skip="no"
            for i in "${XYZ[@]}"
            do
              if [[ "${i}" == "${test}" ]]; then
                echo "<TEST_CASE_ID=${test} RESULT="[SKIP]">"
                lava-test-case "${test}" --result "skip"
                skip="yes"
              fi
            done
            if [[ ${skip} == "no" ]]; then
              echo "<TEST_CASE_ID=${test} RESULT=${result}>"
              if [[ ${result} == "[FAIL]" ]]; then
                lava-test-case "${test}" --result "fail"
              elif [[ ${result} == "[PASS]" ]]; then
                lava-test-case "${test}" --result "pass"
              fi
            fi
        fi
    done < "${RESULT_FILE}"
else
    echo "WARNING: result file is missing!"
fi
